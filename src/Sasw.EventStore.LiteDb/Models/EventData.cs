﻿using System;

namespace Sasw.EventStore.LiteDb.Models
{
    public class EventData
    {
        public long Id { get; set; }
        public string Type { get; set; } = string.Empty;
        public string StreamName { get; set; } = string.Empty;
        public DateTime CreatedOn { get; set; } = DateTime.UtcNow;
        public byte[] Data { get; set; } = new byte[0];
        public byte[] Metadata { get; set; } = new byte[0];
    }
}