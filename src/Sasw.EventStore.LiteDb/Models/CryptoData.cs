﻿namespace Sasw.EventStore.LiteDb.Models
{
    public class CryptoData
    {
        public int Id { get; set; }
        public string SubjectId { get; set; } = string.Empty;
        public byte[] Key { get; set; } = new byte[0];
        public byte[] Nonce { get; set; } = new byte[0];
    }
}
