﻿using System;
using Sasw.EventStore.Abstractions;
using Sasw.EventStore.Common;
using Sasw.EventStore.LiteDb.Models;

namespace Sasw.EventStore.LiteDb.Mappers
{
    public class EventToEventDataMapper
    {
        private readonly JsonSerializer _jsonSerializer;

        public EventToEventDataMapper(
            JsonSerializer jsonSerializer)
        {
            _jsonSerializer = jsonSerializer;
        }

        public EventData Map(IEvent @event, string streamName)
        {
            var eventTypeName = @event.GetType().Name;
            var serializedEvent = _jsonSerializer.Serialize(@event);
            var data = serializedEvent.Data;
            var metadata = serializedEvent.MetaData;
            var eventData =
                new EventData
                {
                    StreamName = streamName,
                    Type = eventTypeName,
                    CreatedOn = DateTime.UtcNow,
                    Data = data,
                    Metadata = metadata
                };

            return eventData;
        }
    }
}
