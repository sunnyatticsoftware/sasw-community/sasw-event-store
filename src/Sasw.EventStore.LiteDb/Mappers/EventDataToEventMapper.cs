﻿using Sasw.EventStore.Abstractions;
using Sasw.EventStore.Common;
using Sasw.EventStore.LiteDb.Models;

namespace Sasw.EventStore.LiteDb.Mappers
{
    public class EventDataToEventMapper
    {
        private readonly JsonSerializer _jsonSerializer;

        public EventDataToEventMapper(
            JsonSerializer jsonSerializer)
        {
            _jsonSerializer = jsonSerializer;
        }

        public IEvent Map(EventData eventData)
        {
            var data = eventData.Data;
            var metadata = eventData.Metadata;
            var eventName = eventData.Type;
            var @event = _jsonSerializer.Deserialize(data, metadata, eventName);
            return @event;
        }
    }
}
