﻿using System;
using LiteDB;
using Sasw.EventStore.LiteDb.Models;

namespace Sasw.EventStore.LiteDb.Factories
{
    public class DatabaseFactory
        : IDisposable
    {
        private readonly LiteDatabase _db;

        public DatabaseFactory(string databasePath, bool isShared = true)
        {
            const string shared = "shared";
            const string direct = "direct";
            var connection = isShared ? shared : direct;
            _db = new LiteDatabase($"FileName={databasePath}; Connection={connection}");
        }

        public ILiteCollection<EventData> CreateEventDataCollection(string collectionName = "events")
        {
            var collection = _db.GetCollection<EventData>(collectionName);
            collection.EnsureIndex(x => x.StreamName);
            return collection;
        }

        public ILiteCollection<CryptoData> CreateCryptoDataCollection(string collectionName = "cryptoKeys")
        {
            var collection = _db.GetCollection<CryptoData>(collectionName);
            collection.EnsureIndex(x => x.SubjectId);
            return collection;
        }

        public void Dispose()
        {
            _db?.Dispose();
        }
    }
}
