﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LiteDB;
using Sasw.EventStore.Abstractions;
using Sasw.EventStore.Common.Exceptions;
using Sasw.EventStore.LiteDb.Mappers;
using Sasw.EventStore.LiteDb.Models;

namespace Sasw.EventStore.LiteDb
{
    public class EventStore
        : IEventStore
    {
        private readonly ILiteCollection<EventData> _eventDataCollection;
        private readonly EventToEventDataMapper _eventToEventDataMapper;
        private readonly EventDataToEventMapper _eventDataToEventMapper;

        public EventStore(
            ILiteCollection<EventData> eventDataCollection, 
            EventToEventDataMapper eventToEventDataMapper, 
            EventDataToEventMapper eventDataToEventMapper)
        {
            _eventDataCollection = eventDataCollection;
            _eventToEventDataMapper = eventToEventDataMapper;
            _eventDataToEventMapper = eventDataToEventMapper;
        }

        public Task SaveEvents(string streamName, int aggregateVersion, IEnumerable<IEvent> events)
        {
            var eventsToPersist = events.ToList();
            if (!eventsToPersist.Any())
            {
                return Task.CompletedTask;
            }
            var currentVersion = GetCurrentStreamVersion(streamName);
            var expectedVersion = aggregateVersion - eventsToPersist.Count;
            var isValid = expectedVersion == currentVersion;
            if (!isValid)
            {
                throw new IncorrectVersionException($"Current stream version is {currentVersion} but expected {expectedVersion}");
            }
            var eventDataCollection =
                eventsToPersist.Select(x => _eventToEventDataMapper.Map(x, streamName));
            _eventDataCollection.InsertBulk(eventDataCollection);
            return Task.CompletedTask;
        }

        public Task<IEnumerable<IEvent>> GetEvents(string streamName)
        {
            var events =
                _eventDataCollection
                    .Query()
                    .Where(x => x.StreamName == streamName)
                    .OrderBy(x => x.Id)
                    .ToList();

            var results =
                events
                    .Select(x => _eventDataToEventMapper.Map(x));
            
            return Task.FromResult(results);
        }

        private int GetCurrentStreamVersion(string streamName)
        {
            var version =
                _eventDataCollection
                    .Query()
                    .Where(x => x.StreamName == streamName)
                    .Count();

            return version;
        }
    }
}
