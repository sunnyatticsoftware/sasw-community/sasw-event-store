﻿using System;
using LiteDB;
using Sasw.EventStore.Common.Contracts.Repositories;
using Sasw.EventStore.Common.Models;
using Sasw.EventStore.LiteDb.Models;

namespace Sasw.EventStore.LiteDb.Repositories
{
    public class CryptoRepository
        : ICryptoRepository
    {
        private readonly ILiteCollection<CryptoData> _cryptoDataCollection;

        public CryptoRepository(ILiteCollection<CryptoData> cryptoDataCollection)
        {
            _cryptoDataCollection = cryptoDataCollection;
        }

        public EncryptionKey GetExistingOrNew(string subjectId, Func<EncryptionKey> keyGenerator)
        {
            var cryptoData = _cryptoDataCollection.FindOne(x => x.SubjectId == subjectId);
            if (cryptoData != null)
            {
                var existingKey = new EncryptionKey(cryptoData.Key, cryptoData.Nonce);
                return existingKey;
            }

            var newEncryptionKey = keyGenerator.Invoke();
            var newCryptoData = 
                new CryptoData
                {
                    SubjectId = subjectId,
                    Key = newEncryptionKey.Key, 
                    Nonce = newEncryptionKey.Nonce
                };
            _cryptoDataCollection.Insert(newCryptoData);
            return newEncryptionKey;
        }

        public EncryptionKey GetExistingOrDefault(string subjectId)
        {
            var cryptoData = _cryptoDataCollection.FindOne(x => x.SubjectId == subjectId);
            if (cryptoData != null)
            {
                var existingKey = new EncryptionKey(cryptoData.Key, cryptoData.Nonce);
                return existingKey;
            }

            return default!;
        }

        public void DeleteEncryptionKey(string subjectId)
        {
            var cryptoData = _cryptoDataCollection.FindOne(x => x.SubjectId == subjectId);
            if (cryptoData != null)
            {
                _cryptoDataCollection.Delete(cryptoData.Id);
            }
        }
    }
}
