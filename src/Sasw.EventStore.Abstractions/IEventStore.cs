﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sasw.EventStore.Abstractions
{
    public interface IEventStore
    {
        Task SaveEvents(string streamName, int aggregateVersion, IEnumerable<IEvent> events);
        Task<IEnumerable<IEvent>> GetEvents(string streamName);
    }
}
