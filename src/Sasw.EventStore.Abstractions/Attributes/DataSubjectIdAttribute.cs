using System;

namespace Sasw.EventStore.Abstractions.Attributes
{
    /**
    * Specifies the PII owner (e.g: the person Id)
    */
    public class DataSubjectIdAttribute 
        : Attribute
    {
    }
}