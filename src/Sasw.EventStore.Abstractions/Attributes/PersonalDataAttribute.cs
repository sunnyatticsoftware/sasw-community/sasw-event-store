using System;

namespace Sasw.EventStore.Abstractions.Attributes
{
    /**
     * Specifies the property that holds PII
     */
    public class PersonalDataAttribute
        : Attribute
    {
    }
}