﻿using System;

namespace Sasw.EventStore.Common.Exceptions
{
    public class IncorrectVersionException
        : Exception
    {
        public IncorrectVersionException(string message)
            : base(message)
        {
        }
    }
}
