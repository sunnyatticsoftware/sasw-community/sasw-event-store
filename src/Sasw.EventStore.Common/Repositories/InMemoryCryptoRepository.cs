﻿using System;
using System.Collections.Generic;
using Sasw.EventStore.Common.Contracts.Repositories;
using Sasw.EventStore.Common.Models;

namespace Sasw.EventStore.Common.Repositories
{
    public class InMemoryCryptoRepository
        : ICryptoRepository
    {
        private readonly IDictionary<string, EncryptionKey> _cryptoStore;

        public InMemoryCryptoRepository()
        {
            _cryptoStore = new Dictionary<string, EncryptionKey>();
        }

        public EncryptionKey GetExistingOrNew(string subjectId, Func<EncryptionKey> keyGenerator)
        {
            var isExisting = _cryptoStore.TryGetValue(subjectId, out var keyStored);
            if (isExisting)
            {
                return keyStored;
            }

            var newEncryptionKey = keyGenerator.Invoke();
            _cryptoStore.Add(subjectId, newEncryptionKey);
            return newEncryptionKey;
        }

        public EncryptionKey GetExistingOrDefault(string subjectId)
        {
            var isExisting = _cryptoStore.TryGetValue(subjectId, out var keyStored);
            if (isExisting)
            {
                return keyStored;
            }

            return default!;
        }

        public void DeleteEncryptionKey(string subjectId)
        {
            _cryptoStore.Remove(subjectId);
        }
    }
}
