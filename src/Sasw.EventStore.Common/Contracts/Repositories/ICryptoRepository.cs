﻿using System;
using Sasw.EventStore.Common.Models;

namespace Sasw.EventStore.Common.Contracts.Repositories
{
    public interface ICryptoRepository
    {
        EncryptionKey GetExistingOrNew(string subjectId, Func<EncryptionKey> keyGenerator);
        EncryptionKey GetExistingOrDefault(string subjectId);
        void DeleteEncryptionKey(string subjectId);
    }
}