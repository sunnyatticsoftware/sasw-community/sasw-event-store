﻿using System;
using System.Collections.Generic;

namespace Sasw.EventStore.Common
{
    public class EventStoreConfiguration
    {
        public IEnumerable<Type> SupportedEventTypes { get; }

        public EventStoreConfiguration(IEnumerable<Type> supportedEventTypes)
        {
            SupportedEventTypes = supportedEventTypes;
        }
    }
}
