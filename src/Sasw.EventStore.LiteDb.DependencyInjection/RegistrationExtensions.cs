﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.DependencyInjection;
using Sasw.EventStore.Abstractions;
using Sasw.EventStore.Common;
using Sasw.EventStore.Common.Contracts.Repositories;
using Sasw.EventStore.Common.Repositories;
using Sasw.EventStore.LiteDb.Factories;
using Sasw.EventStore.LiteDb.Mappers;
using Sasw.EventStore.LiteDb.Repositories;

namespace Sasw.EventStore.LiteDb.DependencyInjection
{
    public static class RegistrationExtensions
    {
        public static IServiceCollection AddEventStoreLiteDb(
            this IServiceCollection services,
            Assembly eventsAssembly,
            string databasePath,
            bool isShared = true,
            bool isPersistentCryptoRepository = false)
        {
            services.AddCommonServices(databasePath, isShared, isPersistentCryptoRepository);

            var types =
                eventsAssembly
                    .GetTypes()
                    .Where(x => x.GetInterfaces().Contains(typeof(IEvent))
                                && !x.IsInterface
                                && !x.IsAbstract);

            services.AddSingleton(
                sp =>
                {
                    var eventStoreConfiguration = new EventStoreConfiguration(types);
                    return eventStoreConfiguration;
                });

            return services;
        }

        public static IServiceCollection AddEventStoreLiteDb(
            this IServiceCollection services,
            IEnumerable<Type> supportedEventTypes,
            string databasePath,
            bool isShared = true,
            bool isPersistentCryptoRepository = false)
        {
            services.AddCommonServices(databasePath, isShared, isPersistentCryptoRepository);
            services.AddSingleton(
                sp =>
                {
                    var eventStoreConfiguration = new EventStoreConfiguration(supportedEventTypes);
                    return eventStoreConfiguration;
                });

            return services;
        }

        private static IServiceCollection AddCommonServices(
            this IServiceCollection services,
            string databasePath,
            bool isShared,
            bool isPersistentCryptoRepository)
        {
            if (isPersistentCryptoRepository)
            {
                services.AddSingleton<ICryptoRepository>(
                    sp =>
                    {
                        var databaseFactory = sp.GetService<DatabaseFactory>();
                        var cryptoDataCollection = databaseFactory.CreateCryptoDataCollection();
                        var cryptoRepository = new CryptoRepository(cryptoDataCollection);
                        return cryptoRepository;
                    });
            }
            else
            {
                services.AddSingleton<ICryptoRepository, InMemoryCryptoRepository>();
            }

            services.AddTransient<EventToEventDataMapper>();
            services.AddTransient<EventDataToEventMapper>();
            services.AddTransient<JsonSerializerSettingsFactory>();
            services.AddTransient<JsonSerializer>();
            services.AddTransient<EncryptorDecryptor>();
            services.AddSingleton(sp => new DatabaseFactory(databasePath, isShared));
            services.AddTransient<IEventStore>(
                sp =>
                {
                    var eventToEventDataMapper = sp.GetService<EventToEventDataMapper>();
                    var eventDataToEventMapper = sp.GetService<EventDataToEventMapper>();
                    var databaseFactory = sp.GetService<DatabaseFactory>();
                    var eventDataCollection = databaseFactory.CreateEventDataCollection();
                    var eventStore =
                        new EventStore(
                            eventDataCollection,
                            eventToEventDataMapper,
                            eventDataToEventMapper);
                    return eventStore;
                });

            return services;
        }
    }
}
