﻿using System;
using Sasw.EventStore.Abstractions;

namespace Sasw.EventStore.LiteDb.IntegrationTests.TestSupport.Events
{
    public class EventThree
        : IEvent
    {
        public Guid AggregateId { get; }

        public EventThree(Guid aggregateId)
        {
            AggregateId = aggregateId;
        }
    }
}
