﻿using System;
using Sasw.EventStore.Abstractions;

namespace Sasw.EventStore.LiteDb.IntegrationTests.TestSupport.Events
{
    public class EventOne
        : IEvent
    {
        public Guid AggregateId { get; }

        public EventOne(Guid aggregateId)
        {
            AggregateId = aggregateId;
        }
    }
}
