﻿using System;
using Sasw.EventStore.Abstractions;

namespace Sasw.EventStore.LiteDb.IntegrationTests.TestSupport.Events
{
    public class EventTwo
        : IEvent
    {
        public Guid AggregateId { get; }

        public EventTwo(Guid aggregateId)
        {
            AggregateId = aggregateId;
        }
    }
}
