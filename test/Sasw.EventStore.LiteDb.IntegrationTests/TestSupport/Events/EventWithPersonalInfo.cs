﻿using System;
using Sasw.EventStore.Abstractions;
using Sasw.EventStore.Abstractions.Attributes;

namespace Sasw.EventStore.LiteDb.IntegrationTests.TestSupport.Events
{
    public class EventWithPersonalInfo
        : IEvent
    {
        public Guid AggregateId { get; set; }

        [DataSubjectId]
        public Guid PersonId { get; set; }

        [PersonalData]
        public string Name { get; set; } = string.Empty;

        [PersonalData]
        public DateTime Birthday { get; set; }

        public Address Address { get; set; } = new Address();
    }

    public class Address
    {
        [PersonalData]
        public string Street { get; set; } = string.Empty;

        [PersonalData]
        public int Number { get; set; }

        public string CountryCode { get; set; } = string.Empty;
    }
}
