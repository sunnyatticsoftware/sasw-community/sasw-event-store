﻿using System;
using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using Sasw.EventStore.Abstractions;
using Sasw.EventStore.Common.Contracts.Repositories;
using Sasw.EventStore.Common.Repositories;
using Sasw.EventStore.LiteDb.DependencyInjection;
using Sasw.EventStore.LiteDb.IntegrationTests.TestSupport.Events;
using Sasw.EventStore.LiteDb.Repositories;
using Sasw.TestSupport;
using Xunit;

namespace Sasw.EventStore.LiteDb.IntegrationTests.DependencyInjectionTests
{
    public static class GetServiceTests
    {
        public class Given_Event_Store_With_LiteDb_Has_Been_Registered_When_Getting_IEventStore
            : Given_When_Then_Test
        {
            private IServiceProvider _sut = null!;
            private IEventStore _result = null!;

            protected override void Given()
            {
                _sut =
                    new ServiceCollection()
                        .AddEventStoreLiteDb(
                            typeof(EventOne).Assembly,
                            "foo.db")
                        .BuildServiceProvider(
                            new ServiceProviderOptions
                            {
                                ValidateOnBuild = true,
                                ValidateScopes = true
                            });
            }

            protected override void When()
            {
                _result = _sut.GetService<IEventStore>();
            }

            [Fact]
            public void Then_It_Should_Resolve_Correctly()
            {
                _result.Should().NotBeNull();
            }

            [Fact]
            public void Then_It_Should_Be_An_EventStore()
            {
                _result.Should().BeAssignableTo<EventStore>();
            }
        }

        public class Given_Event_Store_With_LiteDb_Has_Been_Registered_When_Getting_ICryptoRepository
            : Given_When_Then_Test
        {
            private IServiceProvider _sut = null!;
            private ICryptoRepository _result = null!;

            protected override void Given()
            {
                _sut =
                    new ServiceCollection()
                        .AddEventStoreLiteDb(
                            typeof(EventOne).Assembly,
                            "foo.db")
                        .BuildServiceProvider(
                            new ServiceProviderOptions
                            {
                                ValidateOnBuild = true,
                                ValidateScopes = true
                            });
            }

            protected override void When()
            {
                _result = _sut.GetService<ICryptoRepository>();
            }

            [Fact]
            public void Then_It_Should_Resolve_The_InMemoryCryptoRepository()
            {
                _result.Should().BeAssignableTo<InMemoryCryptoRepository>();
            }
        }

        public class Given_Event_Store_With_LiteDb_Has_Been_Registered_With_Persistent_Crypto_Repository_When_Getting_ICryptoRepository
            : Given_When_Then_Test
        {
            private IServiceProvider _sut = null!;
            private ICryptoRepository _result = null!;

            protected override void Given()
            {
                _sut =
                    new ServiceCollection()
                        .AddEventStoreLiteDb(
                            typeof(EventOne).Assembly,
                            "foo.db",
                            true,
                            true)
                        .BuildServiceProvider(
                            new ServiceProviderOptions
                            {
                                ValidateOnBuild = true,
                                ValidateScopes = true
                            });
            }

            protected override void When()
            {
                _result = _sut.GetService<ICryptoRepository>();
            }

            [Fact]
            public void Then_It_Should_Resolve_The_CryptoRepository()
            {
                _result.Should().BeAssignableTo<CryptoRepository>();
            }
        }
    }
}