﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sasw.TestSupport;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using Sasw.EventStore.Abstractions;
using Sasw.EventStore.Common;
using Sasw.EventStore.Common.Contracts.Repositories;
using Sasw.EventStore.Common.Repositories;
using Sasw.EventStore.LiteDb.DependencyInjection;
using Sasw.EventStore.LiteDb.Factories;
using Sasw.EventStore.LiteDb.IntegrationTests.TestSupport.Events;
using Sasw.EventStore.LiteDb.Mappers;
using Xunit;
using JsonSerializer = Sasw.EventStore.Common.JsonSerializer;

namespace Sasw.EventStore.LiteDb.IntegrationTests.EventStoreTests
{
    public static class GetEventsTests
    {
        public class Given_An_Empty_Stream_When_Getting_Events
            : Given_WhenAsync_Then_Test
        {
            private EventStore _sut = null!;
            private DatabaseFactory _db = null!;
            private string _streamName = string.Empty;
            private IEnumerable<IEvent> _result = Enumerable.Empty<IEvent>();

            protected override void Given()
            {
                _db = new DatabaseFactory("test.db");
                var collection = _db.CreateEventDataCollection();

                var supportedEvents =
                    new List<Type>
                    {
                        typeof(EventOne),
                        typeof(EventTwo),
                        typeof(EventThree)
                    };


                var eventStoreConfiguration =
                    new EventStoreConfiguration(supportedEvents);
                
                var cryptoRepository = new InMemoryCryptoRepository();
                var encryptorDecryptor = new EncryptorDecryptor(cryptoRepository);
                var jsonSerializerSettingsFactory = new JsonSerializerSettingsFactory(encryptorDecryptor);
                var jsonSerializer = new JsonSerializer(jsonSerializerSettingsFactory, eventStoreConfiguration);

                var eventToEventDataMapper = new EventToEventDataMapper(jsonSerializer);
                var eventDataToEventMapper = new EventDataToEventMapper(jsonSerializer);

                _sut = new EventStore(collection, eventToEventDataMapper, eventDataToEventMapper);

                _streamName = Guid.NewGuid().ToString();
            }

            protected override async Task WhenAsync()
            {
                _result = await _sut.GetEvents(_streamName);
            }

            [Fact]
            public void Then_It_Should_Return_An_Empty_Enumerable()
            {
                _result.Should().BeEmpty();
            }

            protected override void Cleanup()
            {
                base.Cleanup();
                _db.Dispose();
            }
        }

        public class Given_A_Stream_With_Three_Events_When_Getting_Events
            : Given_WhenAsync_Then_Test
        {
            private EventStore _sut = null!;
            private DatabaseFactory _db = null!;
            private string _streamName = string.Empty;
            private IEnumerable<IEvent> _result = Enumerable.Empty<IEvent>();
            private IEnumerable<IEvent> _expectedEvents = Enumerable.Empty<IEvent>();

            protected override void Given()
            {
                _db = new DatabaseFactory("test.db");
                var collection = _db.CreateEventDataCollection();

                var supportedEvents =
                    new List<Type>
                    {
                        typeof(EventOne),
                        typeof(EventTwo),
                        typeof(EventThree)
                    };
                
                var eventStoreConfiguration =
                    new EventStoreConfiguration(supportedEvents);

                var cryptoRepository = new InMemoryCryptoRepository();
                var encryptorDecryptor = new EncryptorDecryptor(cryptoRepository);
                var jsonSerializerSettingsFactory = new JsonSerializerSettingsFactory(encryptorDecryptor);
                var jsonSerializer = new JsonSerializer(jsonSerializerSettingsFactory, eventStoreConfiguration);

                var eventToEventDataMapper = new EventToEventDataMapper(jsonSerializer);
                var eventDataToEventMapper = new EventDataToEventMapper(jsonSerializer);

                _sut = new EventStore(collection, eventToEventDataMapper, eventDataToEventMapper);

                var aggregateId = Guid.NewGuid();
                _streamName = aggregateId.ToString();

                var events =
                    new List<IEvent>
                    {
                        new EventOne(aggregateId),
                        new EventTwo(aggregateId),
                        new EventThree(aggregateId)
                    };

                var aggregateVersion = events.Count;

                _sut.SaveEvents(_streamName, aggregateVersion, events).GetAwaiter().GetResult();

                _expectedEvents = events;
            }

            protected override async Task WhenAsync()
            {
                _result = await _sut.GetEvents(_streamName);
            }

            [Fact]
            public void Then_It_Should_Return_The_Expected_Events()
            {
                _result.Should().BeEquivalentTo(_expectedEvents, cfg => cfg.RespectingRuntimeTypes());
            }

            protected override void Cleanup()
            {
                base.Cleanup();
                _db.Dispose();
            }
        }

        public class Given_A_Stream_With_Two_Events_With_Personal_Info_When_Getting_Events
            : Given_WhenAsync_Then_Test
        {
            private EventStore _sut = null!;
            private DatabaseFactory _db = null!;
            private string _streamName = string.Empty;
            private Guid _personIdOne;
            private Guid _personIdTwo;
            private IEnumerable<IEvent> _result = Enumerable.Empty<IEvent>();
            private EventWithPersonalInfo _expectedEventWithPersonalInfoOne = null!;
            private EventWithPersonalInfo _expectedEventWithPersonalInfoTwo = null!;

            protected override void Given()
            {
                _db = new DatabaseFactory("test.db");
                var collection = _db.CreateEventDataCollection();

                var supportedEvents =
                    new List<Type>
                    {
                        typeof(EventWithPersonalInfo)
                    };

                var eventStoreConfiguration =
                    new EventStoreConfiguration(supportedEvents);

                var cryptoRepository = new InMemoryCryptoRepository();
                var encryptorDecryptor = new EncryptorDecryptor(cryptoRepository);
                var jsonSerializerSettingsFactory = new JsonSerializerSettingsFactory(encryptorDecryptor);
                var jsonSerializer = new JsonSerializer(jsonSerializerSettingsFactory, eventStoreConfiguration);

                var eventToEventDataMapper = new EventToEventDataMapper(jsonSerializer);
                var eventDataToEventMapper = new EventDataToEventMapper(jsonSerializer);

                _sut = new EventStore(collection, eventToEventDataMapper, eventDataToEventMapper);

                var aggregateId = Guid.NewGuid();

                _personIdOne = Guid.NewGuid();
                var eventWithPersonalInfoOne =
                    new EventWithPersonalInfo
                    {
                        AggregateId = aggregateId,
                        Name = "Joe Bloggs",
                        Birthday = new DateTime(1984, 1, 1, 0, 0, 0, DateTimeKind.Utc),
                        PersonId = _personIdOne,
                        Address =
                            new Address
                            {
                                Street = "Blue Avenue",
                                Number = 23,
                                CountryCode = "ES"
                            }
                    };

                _personIdTwo = Guid.NewGuid();
                var eventWithPersonalInfoTwo =
                    new EventWithPersonalInfo
                    {
                        AggregateId = aggregateId,
                        Name = "Jane Smith",
                        Birthday = new DateTime(1983, 6, 30, 0, 0, 0, DateTimeKind.Utc),
                        PersonId = _personIdTwo,
                        Address =
                            new Address
                            {
                                Street = "Red Avenue",
                                Number = 24,
                                CountryCode = "US"
                            }
                    };

                var eventsToPersist =
                    new List<IEvent>
                    {
                        eventWithPersonalInfoOne,
                        eventWithPersonalInfoTwo
                    };

                _streamName = aggregateId.ToString();

                var aggregateVersion = eventsToPersist.Count;

                _sut.SaveEvents(_streamName, aggregateVersion, eventsToPersist).GetAwaiter().GetResult();

                _expectedEventWithPersonalInfoOne = eventWithPersonalInfoOne;
                _expectedEventWithPersonalInfoTwo = eventWithPersonalInfoTwo;
            }

            protected override async Task WhenAsync()
            {
                _result = await _sut.GetEvents(_streamName);
            }

            [Fact]
            public void Then_It_Should_Retrieve_Two_Events()
            {
                _result.Should().HaveCount(2);
            }

            [Fact]
            public void Then_It_Should_Have_Decrypted_The_First_EventWithPersonalInfo_Event()
            {
                _result.First().Should().BeEquivalentTo(_expectedEventWithPersonalInfoOne);
            }

            [Fact]
            public void Then_It_Should_Have_Decrypted_The_Second_EventWithPersonalInfo_Event()
            {
                _result.Last().Should().BeEquivalentTo(_expectedEventWithPersonalInfoTwo);
            }

            protected override void Cleanup()
            {
                base.Cleanup();
                _db.Dispose();
            }
        }

        public class Given_A_Stream_With_Two_Events_With_Personal_Info_And_Encryption_Key_For_The_Second_One_Is_Deleted_When_Getting_Events
            : Given_WhenAsync_Then_Test
        {
            private EventStore _sut = null!;
            private DatabaseFactory _db = null!;
            private string _streamName = string.Empty;
            private Guid _personIdOne;
            private Guid _personIdTwo;
            private IEnumerable<IEvent> _result = Enumerable.Empty<IEvent>();
            private EventWithPersonalInfo _expectedEventWithPersonalInfoOne = null!;
            private EventWithPersonalInfo _expectedEventWithPersonalInfoTwo = null!;

            protected override void Given()
            {
                _db = new DatabaseFactory("test.db");
                var collection = _db.CreateEventDataCollection();

                var supportedEvents =
                    new List<Type>
                    {
                        typeof(EventWithPersonalInfo)
                    };

                var eventStoreConfiguration =
                    new EventStoreConfiguration(supportedEvents);

                var cryptoRepository = new InMemoryCryptoRepository();
                var encryptorDecryptor = new EncryptorDecryptor(cryptoRepository);
                var jsonSerializerSettingsFactory = new JsonSerializerSettingsFactory(encryptorDecryptor);
                var jsonSerializer = new JsonSerializer(jsonSerializerSettingsFactory, eventStoreConfiguration);

                var eventToEventDataMapper = new EventToEventDataMapper(jsonSerializer);
                var eventDataToEventMapper = new EventDataToEventMapper(jsonSerializer);

                _sut = new EventStore(collection, eventToEventDataMapper, eventDataToEventMapper);

                var aggregateId = Guid.NewGuid();

                _personIdOne = Guid.NewGuid();
                var eventWithPersonalInfoOne =
                    new EventWithPersonalInfo
                    {
                        AggregateId = aggregateId,
                        Name = "Joe Bloggs",
                        Birthday = new DateTime(1984, 1, 1, 0, 0, 0, DateTimeKind.Utc),
                        PersonId = _personIdOne,
                        Address =
                            new Address
                            {
                                Street = "Blue Avenue",
                                Number = 23,
                                CountryCode = "ES"
                            }
                    };

                _personIdTwo = Guid.NewGuid();
                var eventWithPersonalInfoTwo =
                    new EventWithPersonalInfo
                    {
                        AggregateId = aggregateId,
                        Name = "Jane Smith",
                        Birthday = new DateTime(1983, 6, 30, 0, 0, 0, DateTimeKind.Utc),
                        PersonId = _personIdTwo,
                        Address =
                            new Address
                            {
                                Street = "Red Avenue",
                                Number = 24,
                                CountryCode = "US"
                            }
                    };

                var eventsToPersist =
                    new List<IEvent>
                    {
                        eventWithPersonalInfoOne,
                        eventWithPersonalInfoTwo
                    };

                _streamName = aggregateId.ToString();

                var aggregateVersion = eventsToPersist.Count;

                _sut.SaveEvents(_streamName, aggregateVersion, eventsToPersist).GetAwaiter().GetResult();

                cryptoRepository.DeleteEncryptionKey(_personIdTwo.ToString());

                _expectedEventWithPersonalInfoOne = eventWithPersonalInfoOne;
                _expectedEventWithPersonalInfoTwo =
                    new EventWithPersonalInfo
                    {
                        AggregateId = aggregateId,
                        Name = "***",
                        Birthday = default,
                        PersonId = _personIdTwo,
                        Address =
                            new Address
                            {
                                Street = "***",
                                Number = default,
                                CountryCode = "US"
                            }
                    };
            }

            protected override async Task WhenAsync()
            {
                _result = await _sut.GetEvents(_streamName);
            }

            [Fact]
            public void Then_It_Should_Retrieve_Two_Events()
            {
                _result.Should().HaveCount(2);
            }

            [Fact]
            public void Then_It_Should_Have_Decrypted_The_First_EventWithPersonalInfo_Event()
            {
                _result.First().Should().BeEquivalentTo(_expectedEventWithPersonalInfoOne);
            }

            [Fact]
            public void Then_It_Should_Have_Masked_The_Second_EventWithPersonalInfo_Event()
            {
                _result.Last().Should().BeEquivalentTo(_expectedEventWithPersonalInfoTwo);
            }

            protected override void Cleanup()
            {
                base.Cleanup();
                _db.Dispose();
            }
        }

        public class Given_Event_Store_Registered_In_Di_Container_And_A_Stream_With_Two_Events_With_Personal_Info_And_Persistent_Crypto_Repository_And_Encryption_Key_For_The_Second_One_Is_Deleted_When_Getting_Events
            : Given_WhenAsync_Then_Test
        {
            private IEventStore _sut = null!;
            private string _streamName = string.Empty;
            private Guid _personIdOne;
            private Guid _personIdTwo;
            private IEnumerable<IEvent> _result = Enumerable.Empty<IEvent>();
            private EventWithPersonalInfo _expectedEventWithPersonalInfoOne = null!;
            private EventWithPersonalInfo _expectedEventWithPersonalInfoTwo = null!;

            protected override void Given()
            {
                var supportedEvents =
                    new List<Type>
                    {
                        typeof(EventWithPersonalInfo)
                    };

                var serviceProvider =
                    new ServiceCollection()
                        .AddEventStoreLiteDb(
                            supportedEvents,
                            "test.db",
                            true,
                            true)
                        .BuildServiceProvider(
                            new ServiceProviderOptions
                            {
                                ValidateOnBuild = true,
                                ValidateScopes = true
                            });

                _sut = serviceProvider.GetService<IEventStore>();

                var aggregateId = Guid.NewGuid();

                _personIdOne = Guid.NewGuid();
                var eventWithPersonalInfoOne =
                    new EventWithPersonalInfo
                    {
                        AggregateId = aggregateId,
                        Name = "Joe Bloggs",
                        Birthday = new DateTime(1984, 1, 1, 0, 0, 0, DateTimeKind.Utc),
                        PersonId = _personIdOne,
                        Address =
                            new Address
                            {
                                Street = "Blue Avenue",
                                Number = 23,
                                CountryCode = "ES"
                            }
                    };

                _personIdTwo = Guid.NewGuid();
                var eventWithPersonalInfoTwo =
                    new EventWithPersonalInfo
                    {
                        AggregateId = aggregateId,
                        Name = "Jane Smith",
                        Birthday = new DateTime(1983, 6, 30, 0, 0, 0, DateTimeKind.Utc),
                        PersonId = _personIdTwo,
                        Address =
                            new Address
                            {
                                Street = "Red Avenue",
                                Number = 24,
                                CountryCode = "US"
                            }
                    };

                var eventsToPersist =
                    new List<IEvent>
                    {
                        eventWithPersonalInfoOne,
                        eventWithPersonalInfoTwo
                    };

                _streamName = aggregateId.ToString();

                var aggregateVersion = eventsToPersist.Count;

                _sut.SaveEvents(_streamName, aggregateVersion, eventsToPersist).GetAwaiter().GetResult();

                var cryptoRepository = serviceProvider.GetService<ICryptoRepository>();
                cryptoRepository.DeleteEncryptionKey(_personIdTwo.ToString());

                _expectedEventWithPersonalInfoOne = eventWithPersonalInfoOne;
                _expectedEventWithPersonalInfoTwo =
                    new EventWithPersonalInfo
                    {
                        AggregateId = aggregateId,
                        Name = "***",
                        Birthday = default,
                        PersonId = _personIdTwo,
                        Address =
                            new Address
                            {
                                Street = "***",
                                Number = default,
                                CountryCode = "US"
                            }
                    };
            }

            protected override async Task WhenAsync()
            {
                _result = await _sut.GetEvents(_streamName);
            }

            [Fact]
            public void Then_It_Should_Retrieve_Two_Events()
            {
                _result.Should().HaveCount(2);
            }

            [Fact]
            public void Then_It_Should_Have_Decrypted_The_First_EventWithPersonalInfo_Event()
            {
                _result.First().Should().BeEquivalentTo(_expectedEventWithPersonalInfoOne);
            }

            [Fact]
            public void Then_It_Should_Have_Masked_The_Second_EventWithPersonalInfo_Event()
            {
                _result.Last().Should().BeEquivalentTo(_expectedEventWithPersonalInfoTwo);
            }
        }
    }
}