﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sasw.TestSupport;
using System.Threading.Tasks;
using FluentAssertions;
using Sasw.EventStore.Abstractions;
using Sasw.EventStore.Common;
using Sasw.EventStore.Common.Repositories;
using Sasw.EventStore.LiteDb.Factories;
using Sasw.EventStore.LiteDb.IntegrationTests.TestSupport.Events;
using Sasw.EventStore.LiteDb.Mappers;
using Xunit;
using JsonSerializer = Sasw.EventStore.Common.JsonSerializer;

namespace Sasw.EventStore.LiteDb.IntegrationTests.EventStoreTests
{
    public static class SaveEventsTests
    {
        public class Given_An_Empty_Stream_And_Three_Events_When_Saving_Events
            : Given_WhenAsync_Then_Test
        {
            private EventStore _sut = null!;
            private DatabaseFactory _db = null!;
            private string _streamName = string.Empty;
            private int _aggregateVersion;
            private IEnumerable<IEvent> _events = Enumerable.Empty<IEvent>();
            private Exception _exception = null!;

            protected override void Given()
            {
                _db = new DatabaseFactory("test.db");
                var collection = _db.CreateEventDataCollection();

                var supportedEvents =
                    new List<Type>
                    {
                        typeof(EventOne),
                        typeof(EventTwo),
                        typeof(EventThree)
                    };


                var eventStoreConfiguration =
                    new EventStoreConfiguration(supportedEvents);
                
                var cryptoRepository = new InMemoryCryptoRepository();
                var encryptorDecryptor = new EncryptorDecryptor(cryptoRepository);
                var jsonSerializerSettingsFactory = new JsonSerializerSettingsFactory(encryptorDecryptor);
                var jsonSerializer = new JsonSerializer(jsonSerializerSettingsFactory, eventStoreConfiguration);

                var eventToEventDataMapper = new EventToEventDataMapper(jsonSerializer);
                var eventDataToEventMapper = new EventDataToEventMapper(jsonSerializer);

                _sut = new EventStore(collection, eventToEventDataMapper, eventDataToEventMapper);

                var aggregateId = Guid.NewGuid();
                _streamName = aggregateId.ToString();
                _events =
                    new List<IEvent>
                    {
                        new EventOne(aggregateId),
                        new EventTwo(aggregateId),
                        new EventThree(aggregateId)
                    };

                _aggregateVersion = _events.Count();
            }

            protected override async Task WhenAsync()
            {
                try
                {
                    await _sut.SaveEvents(_streamName, _aggregateVersion, _events);
                }
                catch (Exception exception)
                {
                    _exception = exception;
                }
            }

            [Fact]
            public void Then_It_Should_Not_Throw_Any_Exception()
            {
                _exception.Should().BeNull();
            }

            protected override void Cleanup()
            {
                base.Cleanup();
                _db.Dispose();
            }
        }
    }
}